---
layout: page
title: Help & troubleshooting
permalink: /help/
order: 7
---

# Contact
If you need help, don't hesitate to send us (`lcsb-sysadmins (at) uni.lu`) an email, we will try to assist you as soon as possible.

# Troubleshooting

 * ##### The website looks broken! There are no images, no colors etc.
You probably didn't configure `baseurl` parameter in the settings or configured it wrongly. Please take a look on `_settings.yml` file.

* ##### Soemtimes Markdown does not work with HTML or vice versa
Ensure that you don't mix these two, and there's always one lien break between these two. For example:

This won't work:
```
<strong>This is bold</strong>[link](https://example.com). No line break between this and next line.
*Italics*
```

And this will:

```
<strong>This is bold</strong>[link](https://example.com). Line break after this line:

*Italics*
```

 * ##### The website is not updated after commiting to the repository!
Did you push the commit? If yes, then you probably changed/deleted `.gitlab-ci.yml` file.

 * ##### The links in the menu are not working (they point to "404: Not found").
You probably didn't add `permalink` attribute. Or the post has `published: false` or `draft: true` set. Please take a look on the post file.

 * ##### Something goes wrong with Gitlab-CI
It never happened before, please notify us immediately.
