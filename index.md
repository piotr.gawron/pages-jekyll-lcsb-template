---
layout: default
title: Index
order: 1
---

# Hi! Welcome to the example page

On this website you can find a detailed instruction on **how to set-up and operate your own Jekyll* website** using Gitlab-CI* and LCSB template*. 

<small>* That's a lot of technology names, but don't worry - it's all very simple to use! :)</small>

<p>&nbsp;</p>

### 1. How to create such a website?
We have created [a detailed step-by-step guide](guide). It'll take less than 10 minutes to start!

**You provide us the content (text), and we'll do the rest.**

### 2. What are the requirements?
You need to be LCSB employee (students count too, don't worry :)) and have a valid Gitlab account (<https://gitlab.lcsb.uni.lu/>).

Some knowledge about [Markdown](https://www.markdownguide.org/getting-started/) and [Git](https://git-scm.com/) will definitely help, but is not required to start.

### 3. How does it all work?
It boils down to a few _key technologies_:
 * **Markdown** is an easy text format.
 * **Jekyll** transforms Markdown documents into web sites.
 * **LCSB theme** contains approved layouts and logos of University and LCSB.
 * **Git** allows for versioning of the files and stores them together in a repository.
 * **Gitlab** stores the Git repositories on LCSB servers.
 * **Gitlab CI** is capable of running programs every time you push new commits to Git repository.
 * **Gitlab pages** can automatically publish web sites to the Internet.

<small>In short - every time you commit new changes to a Git repository forked from our template, Gitlab CI triggers a task, which runs Jekyll to produce a web site based on your repository contents. Once this is done, Gitlab Pages publishes the webpage to the internet.</small>

### 4. How to...? (add images, add new page etc.)
Please take a look on [the documentation](howto).

### 5. Something does not work?
Please take a look on our [troubleshooting guide](help).

### 6. Is there any other problem?
Please feel free to send us an email! (`lcsb-sysadmins (at) uni.lu`)

<p>&nbsp;</p>

Sources for this very page are [available in Gitlab](https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template).

Feel free to inspect [the repository containing our LCSB theme](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default), if you are interested.

If you have found any issues with the layout, have a suggestion or any request, please [open a new issue](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
