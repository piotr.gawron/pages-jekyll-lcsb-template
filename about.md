---
layout: page
title: About
permalink: /about/
order: 10
---

## The people actively engaged in the development are:

* Jacek Lebioda (jacek.lebioda@uni.lu)
* Laurent Heirendt
* Yohan Jarosz

## We've accepted contributions from:

* Christophe Trefois
* Elisabeth Guerard
* Mirek Kratochvil

---

## Jekyll 
You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

Sources for this very page are [available in Gitlab](https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template).

You can find the source code for this theme at Gitlab, too:
[core-services/jekyll-theme-lcsb-default](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default).

## Other websites
We have successfully helped in publishing at least the following websites:
 * [How-to cards](https://howto.lcsb.uni.lu/)
 * [Page of theme](https://core-services.pages.uni.lu/jekyll-theme-lcsb-default/)
 * [This very page](https://core-services.pages.uni.lu/pages-jekyll-lcsb-template/)
 * [Minerva doc's](https://minerva.pages.uni.lu/doc/)
 * [PDmap blog](https://pdmap.pages.uni.lu/blog/)
 * [TGM pipeline](https://r3lab.uni.lu/web/tgm-pipeline/)
 * [IMP](https://imp.pages.uni.lu/web/)
 * [IMP, second deployment](https://r3lab.uni.lu/web/imp/)
 * [CaSiAn](https://r3lab.uni.lu/web/casa/)
 * [MIC-MAC](https://isc.pages.uni.lu/micmac/)
 * [R3lab](https://r3lab.uni.lu/)
 * [LIMS front page](https://lims.uni.lu/)
 * [ELIXIR website](https://elixir-luxembourg.org) (this one is based on different template though)
