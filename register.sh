#!/usr/bin/env bash

# The data will be sent to this URL with POST request
REGISTER_URL="${REGISTER_URL:=https://10.240.6.12/register}"

# Debug flag
DEBUG=${DEBUG:=false}

# MD5 checksum of register script
VERSION=`cat register.sh | md5sum | cut -d ' ' -f 1`

# Get envvars which start with "CI" or "GITLAB", remove the ones containing "token" or "password"
# and then transform 'key=value' into '"key":"value",' and append version (md5 hash)
COLLECTED_INFO=$(printenv | grep 'CI\|GITLAB' | grep -v 'token\|password\|jwt' | echo {$(sed -e 's/^/\"/g; s/$/\", /g; s/=/\":\"/g;')\"v\":\"$VERSION\"})

# To avoid mess with double apostrophes, encode using base64
ENCODED_INFO=$(echo $COLLECTED_INFO | base64 | tr -d ' \n')

# Debug messages
if [[ "$DEBUG" == "true" ]]; then
    echo $COLLECTED_INFO
    echo $ENCODED_INFO
    echo "curl -X POST -kL -H \"Content-Type: text/base64\" -d $ENCODED_INFO $REGISTER_URL"
fi;

# Making an actual request; follow redirects and allow insecure certificates.
curl -X POST -kL -H "Content-Type: text/base64" -d $ENCODED_INFO $REGISTER_URL

# Return success
exit 0
